# Bitbucket Pipelines Task: GCP app engine deploy

Task to deploy to [GCP app engine](https://cloud.google.com/sdk/gcloud/reference/app/deploy).

Deploys an application to google app engine.

## Usage

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- task: atlassian/gcp-app-engine-deploy:0.1.0
  environment:
    KEY_FILE: <string>
    PROJECT: <string>
    # VERSION: <string> # Optional
    # BUCKET: <string> # Optional
    # IMAGE: <string> # Optional
    # PROMOTE: <boolean> # Optional
    # STOP_PREVIOUS_VERSION: <boolean> # Optional
    # EXTRA_ARGS: <string> # Optional
```

## Parameters

| Environment                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| KEY_FILE (*)                  |  base64 encoded Key file for a [GCP service account](https://cloud.google.com/iam/docs/creating-managing-service-account-keys). |
| PROJECT (*)                   |  The project that owns the app to deploy. |
| VERSION                       |  The version of the app to be created/replaced. |
| BUCKET                        |  The google cloud storage bucket to store the files associated with the deployment. |
| IMAGE                         |  Deploy with a specific GCR docker image. |
| PROMOTE                       |  If true the deployed version is receives all traffic, false to not receive traffic. |
| STOP_PREVIOUS_VERSION         |  If true the previous version receiving traffic is stopped, false to not stop the previous version. |
| EXTRA_ARGS                    |  Extra arguments to be passed to the CLI (see [GCP app deploy docs](https://cloud.google.com/sdk/gcloud/reference/app/deploy) for more details). |

_(*) = required parameter._

More info about parameters and values can be found in the AWS official documentation: https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html

## Examples

Basic example:

```yaml
script:
  - task: atlassian/gcp-app-engine-deploy:0.1.0
    environment:
      KEY_FILE: ${KEY_FILE}
      PROJECT: "my-project"
```

Advanced example: 
    
```yaml
script:
  - task: atlassian/gcp-app-engine-deploy:0.1.0
    environment:
      KEY_FILE: ${KEY_FILE}
      PROJECT: "my-project"
      VERSION: "alpha"
      BUCKET: "gs://my-bucket"
      IMAGE: "gcr.io/my/image"
      PROMOTE: "true"
      STOP_PREVIOUS_VERSION: "true"
      EXTRA_ARGS: "--logging=debug"
```

## Bugs
Issues, bugs and feature requests can be reported via the [Bitbucket Cloud public issue tracker][sitemaster].

When reporting bugs or issues, please provide:

* The version of the task
* Relevant logs and error messages
* Steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[sitemaster]: https://bitbucket.org/site/master
