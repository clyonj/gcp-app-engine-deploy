#!/usr/bin/env bats

# Test GCP app engine, https://cloud.google.com/appengine/

set -e

setup() {
  IMAGE_NAME=${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}
  RANDOM_NUMBER=$RANDOM
  sed s/{{BODY}}/${RANDOM_NUMBER}/g test/app.js.template >> test/app.js
  sed s/{{BODY}}/${RANDOM_NUMBER}/g test/package.json.template >> test/package.json
}

teardown() {
  rm test/app.js
  rm test/package.json
}

@test "upload to GCP app engine" {
  # execute tests
  run docker run \
    -e KEY_FILE="${GCP_KEY_FILE}" \
    -e PROJECT="civil-array-221002" \
    -e VERSION="${BITBUCKET_BUILD_NUMBER}" \
    -e PROMOTE="true" \
    -e STOP_PREVIOUS_VERSION="true" \
    -v $(pwd):$(pwd) \
    -w $(pwd)/test \
    $IMAGE_NAME

  [[ "${status}" == "0" ]]

  # verify
  run curl --silent "https://civil-array-221002.appspot.com"
  [[ "${status}" == "0" ]]
  [[ "${output}" == "${RANDOM_NUMBER}" ]]
}
