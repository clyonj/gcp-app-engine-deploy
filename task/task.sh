#!/usr/bin/env bash

# Deploy to GCP app engine, https://cloud.google.com/appengine/
#
# Required globals:
#   KEY_FILE
#   PROJECT
#
# Optional globals:
#   VERSION
#   BUCKET
#   IMAGE
#   PROMOTE
#   STOP_PREVIOUS_VERSION
#   EXTRA_ARGS

source "$(dirname "$0")/common.sh"

# mandatory parameters
KEY_FILE=${KEY_FILE:?'KEY_FILE environment variable missing.'}
PROJECT=${PROJECT:?'PROJECT environment variable missing.'}

info "Setting up environment".

run "echo ${KEY_FILE} | base64 -d >> /tmp/key-file.json"
run gcloud auth activate-service-account --key-file /tmp/key-file.json --quiet
run gcloud config set project $PROJECT --quiet

ARGS_STRING=""

if [ ! -z "${VERSION}" ]; then
  ARGS_STRING="${ARGS_STRING} --version ${VERSION} "
fi

if [ ! -z "${BUCKET}" ]; then
  ARGS_STRING="${ARGS_STRING} --bucket ${BUCKET} "
fi

if [ ! -z "${IMAGE}" ]; then
  ARGS_STRING="${ARGS_STRING} --image-url ${IMAGE} "
fi

if [ ! -z "${PROMOTE}" ]; then
  if [ "${PROMOTE}" == "true" ]; then
    ARGS_STRING="${ARGS_STRING} --promote "
  elif [ "${PROMOTE}" == "false" ]; then
    ARGS_STRING="${ARGS_STRING} --no-promote "
  else
    fail "Value of promote: ${PROMOTE}, has to be true | false."
  fi
fi

if [ ! -z "${STOP_PREVIOUS_VERSION}" ]; then
  if [ "${STOP_PREVIOUS_VERSION}" == "true" ]; then
    ARGS_STRING="${ARGS_STRING} --stop-previous-version "
  elif [ "${STOP_PREVIOUS_VERSION}" == "false" ]; then
    ARGS_STRING="${ARGS_STRING} --no-stop-previous-version "
  else
    fail "Value of stop_previous_version: ${STOP_PREVIOUS_VERSION}, has to be true | false."
  fi
fi

ARGS_STRING="${ARGS_STRING} ${EXTRA_ARGS:=""}"

info "Starting deployment to GCP app engine..."

run gcloud app deploy --quiet ${ARGS_STRING}

if [ "${status}" -eq 0 ]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi
