# Changelog
During the Alpha period, minor version releases in the 0.x.y range may introduce breaking changes to the task's interface. 

## [0.1.0] - 2018-10-25  - Alpha Release
### Added
Initial release of Bitbucket Pipelines GCP app engine deploy task.
